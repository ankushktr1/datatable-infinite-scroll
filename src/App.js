import React from "react";
import "./App.css";
import DatatableList from "./components/DatatableList";

/*
 * Access DatatableList Component with columns and rows props in App component
 * When rows props not passes than api will call with 500 record and list data in table
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <DatatableList
          columns={[
            {
              id: "id",
              label: "Id",
              numeric: true,
              width: ""
            },
            {
              id: "title",
              label: "Title",
              numeric: false,
              width: 350
            },
            {
              id: "url",
              label: "Url",
              numeric: false,
              width: 350
            },
            {
              id: "thumbnailUrl",
              label: "Thumbnail Url",
              numeric: false,
              width: 350
            }
          ]}

          // Uncomment this if you don't want to use api and use rows as props
          /*
            rows={[
              {
                id: 1,
                title: "Hello",
                url: "https://via.placeholder.com/600/92c952",
                thumbnailUrl: "https://via.placeholder.com/600/92c952"
              }
            ]} 
          */
        />
      </header>
    </div>
  );
}

export default App;
