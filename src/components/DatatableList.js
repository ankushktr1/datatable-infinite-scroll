import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { withStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import { InfiniteLoader, Column, Table } from "react-virtualized";
import { fetchUsersPics } from "../redux/fetchrecords/FetchRecordsActions";
import { store } from "../redux/Store";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";

/*
 * Apply style in table using detault theme object
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

const styles = theme => ({
  flexContainer: {
    display: "flex",
    alignItems: "center",
    boxSizing: "border-box"
  },
  tableCell: {
    flex: 1
  },
  noClick: {
    cursor: "initial"
  }
});

class DatatableList extends React.Component {
  /*
   * Define headerHeight and rowHeight as default props
   */

  static defaultProps = {
    headerHeight: 50,
    rowHeight: 50
  };

  /*
   * Dispatch action based on props rows from DatatableList coponent
   */

  componentDidMount() {
    if (
      !this.props.rows ||
      (this.props.rows && typeof this.props.rows !== "object")
    ) {
      store.dispatch(fetchUsersPics());
    }
  }

  /*
   * CSS class to apply to all table rows (including the header row)
   */

  getRowClassName = ({ index }) => {
    const { classes, onRowClick } = this.props;

    return clsx(classes.tableRow, classes.flexContainer, {
      [classes.tableRowHover]: index !== -1 && onRowClick != null
    });
  };

  /*
   * Callback responsible for rendering a cell's contents
   */

  cellRenderer = ({ cellData, columnIndex }) => {
    const { columns, classes, rowHeight, onRowClick } = this.props;
    return (
      <TableCell
        component="div"
        className={clsx(classes.tableCell, classes.flexContainer, {
          [classes.noClick]: onRowClick == null
        })}
        variant="body"
        style={{ height: rowHeight }}
        align={
          (columnIndex != null && columns[columnIndex].numeric) || false
            ? "right"
            : "left"
        }
      >
        {cellData}
      </TableCell>
    );
  };

  /*
   * callback responsible for rendering a column's header column
   */

  headerRenderer = ({ label, columnIndex }) => {
    const { headerHeight, columns, classes } = this.props;

    return (
      <TableCell
        component="div"
        className={clsx(
          classes.tableCell,
          classes.flexContainer,
          classes.noClick
        )}
        variant="head"
        style={{ height: headerHeight }}
        align={columns[columnIndex].numeric || false ? "right" : "left"}
      >
        <span>{label}</span>
      </TableCell>
    );
  };

  /*
   * Load data via api call after scroll in window based on rows props not provide by DatatablList component
   */

  loadMoreRows = ({ startIndex }) => {
    if (
      !this.props.rows ||
      (this.props.rows && typeof this.props.rows !== "object")
    ) {
      store.dispatch(fetchUsersPics(startIndex));
    }
  };

  render() {
    // destructuring props of component
    const {
      classes,
      columns,
      rowHeight,
      headerHeight,
      ...tableProps
    } = this.props;

    return this.props.listProducts.loading ? (
      <Loader
        type="Puff"
        color="#00BFFF"
        height={100}
        width={100}
        timeout={5000}
      />
    ) : (
      <>
        <h2 style={{ color: "black", borderBottom: "2px solid black" }}>
          Scrollable Infinite List Datatable
        </h2>
        <InfiniteLoader
          isRowLoaded={({ index }) => !!this.props.listProducts.users[index]}
          loadMoreRows={this.loadMoreRows}
          rowCount={50000}
        >
          {({ onRowsRendered, registerChild }) => (
            <Table
              height={700}
              width={1200}
              rowHeight={rowHeight}
              gridStyle={{
                direction: "inherit"
              }}
              headerHeight={headerHeight}
              className={classes.table}
              {...tableProps}
              rowClassName={this.getRowClassName}
              onRowsRendered={onRowsRendered}
              ref={registerChild}
              rowCount={
                !this.props.rows ||
                (this.props.rows && typeof this.props.rows !== "object")
                  ? this.props.listProducts.users.length
                  : this.props.rows.length
              }
              rowGetter={({ index }) =>
                !this.props.rows ||
                (this.props.rows && typeof this.props.rows !== "object")
                  ? this.props.listProducts.users[index]
                  : this.props.rows[index]
              }
              onRowClick={({ event, index, rowData }) => {
                alert(`You have clicked row id ${rowData.id} `);
              }}
            >
              {this.props.columns.map(({ id, width, ...rest }, index) => {
                return (
                  <Column
                    key={id}
                    headerRenderer={headerProps =>
                      this.headerRenderer({
                        ...headerProps,
                        columnIndex: index
                      })
                    }
                    className={classes.flexContainer}
                    cellRenderer={this.cellRenderer}
                    dataKey={id}
                    width={
                      typeof width === "string" || typeof width === undefined
                        ? 350
                        : width
                    }
                    {...rest}
                  />
                );
              })}
            </Table>
          )}
        </InfiniteLoader>
      </>
    );
  }
}

/*
 * Define Proptypes as isRequired,bool,number,func
 */

DatatableList.propTypes = {
  classes: PropTypes.object.isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      numeric: PropTypes.bool
    })
  ).isRequired,
  headerHeight: PropTypes.number,
  onRowClick: PropTypes.func,
  rowHeight: PropTypes.number
};

/*
 * Access updated state from Redux Store
 */

const mapStateToProps = state => {
  return {
    listProducts: state
  };
};

/*
 * Connect component with redux store using connect HOC and apply styles on table
 */

export default withStyles(styles)(
  connect(mapStateToProps, null)(DatatableList)
);
