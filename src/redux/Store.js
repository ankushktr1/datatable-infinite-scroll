import { listReducer } from "./fetchrecords/FetchRecordsReducer";
import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk";
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/*
 * Create redux store using createStore of redux
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

export const store = createStore(
  listReducer,
  composeEnhancer(applyMiddleware(ReduxThunk))
);
