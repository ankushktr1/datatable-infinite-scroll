import {
  STORE_LIST_SUCCESS,
  STORE_LIST_ERROR,
  STORE_LIST_FETCH
} from "./FetchRecordsTypes";
import axios from "axios";
import { config } from "../../Config";

/*
 * Function for show loader on api call
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

const storeListFetch = () => {
  return {
    type: STORE_LIST_FETCH
  };
};

/*
 * Function for all details from api
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

const storeListSuccess = data => {
  return {
    type: STORE_LIST_SUCCESS,
    payload: data
  };
};

/*
 * Function for fetch error via api call
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

const storeListError = error => {
  return {
    type: STORE_LIST_ERROR,
    payload: error
  };
};

/*
 * Action creator fetchUserPics which return function for call api
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

export const fetchUsersPics = (start = 0, param) => {
  return function(dispatch) {
    dispatch(storeListFetch());
    axios
      .get(`${config.API_URL}?_start=${start}&_limit=500`)
      .then(response => {
        setTimeout(() => {
          dispatch(storeListSuccess(response.data));
        }, 2000);
      })
      .catch(error => {
        dispatch(storeListError(error.message));
      });
  };
};
