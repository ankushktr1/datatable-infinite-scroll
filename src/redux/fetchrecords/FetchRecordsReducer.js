import {
  STORE_LIST_SUCCESS,
  STORE_LIST_ERROR,
  STORE_LIST_FETCH
} from "./FetchRecordsTypes";

/*
 * Define InitialState for reducer
 */

const initialState = {
  users: [],
  loading: false,
  error: ""
};

/*
 * listReducer for set loader, fetch records from api and display error message
 * @author [Ankush Katiyar](https://github.com/ankushktr)
 */

export const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case STORE_LIST_FETCH:
      return {
        ...state,
        loading: true
      };
    case STORE_LIST_SUCCESS:
      return {
        ...state,
        users: state.users.concat(action.payload),
        loading: false
      };
    case STORE_LIST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
