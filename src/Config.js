const prod = {
  API_URL: "https://jsonplaceholder.typicode.com/photos"
};

const dev = {
  API_URL: "https://jsonplaceholder.typicode.com/photos"
};

export const config = process.env.NODE_ENV === "development" ? dev : prod;
